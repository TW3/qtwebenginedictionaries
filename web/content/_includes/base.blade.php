<!DOCTYPE html>
<html lang="en-GB">

	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="@yield('pageDescription', $siteDescription)" />

		<title>{{$siteName}} @yield('pageTitle')</title>

		<link rel="shortcut icon" href="@url('assets/images/favicon.ico')" type="image/x-icon" sizes="16x16 32x32"/>
		<link rel="stylesheet" href="@url('assets/css/spectre.min.css')" />
		<link rel="stylesheet" href="@url('assets/css/spectre-icons.min.css')" />
		<link rel="stylesheet" href="@url('assets/css/all.css')" />
	</head>

	<body>

		<div class="container">
			<div class="columns">
				<div class="column col-12"><div class="p-centered"><a href="@url('/')" class="internal-link"><img src="@url('assets/images/qtwebenginedictionaries.svg')" class="img-responsive height-128" alt="qtwebenginedictionaries Logo" /></a></div></div>
			</div>
		</div>

		<div class="container">
			<div class="columns">
				<div class="column col-8">@yield('body')
@php
	if (isset($jsonData)) {
		// Placeholder
	} else {
    	$jsonData = '../../dicts.json';
    	$jsonDataCont = file_get_contents($jsonData);
		$jsonDataContents = preg_replace('/\x{FEFF}/u', '', $jsonDataCont); // Remove any BOM characters from the data
    	$jsonDecode = json_decode($jsonDataContents);
    	$jsonLicData = '../../licenses.json';
    	$jsonLicDataCont = file_get_contents($jsonLicData);
		$jsonLicDataContents = preg_replace('/\x{FEFF}/u', '', $jsonLicDataCont);
    	$jsonLicDecode = json_decode($jsonLicDataContents);
		if ($jsonDecode != false) {
			if (function_exists('sort_by_description')){
				// Placeholder 
			} else {
				function sort_by_description( $a, $b ) { 
  					if(  $a->description ==  $b->description ){ return 0 ; } 
  						return ($a->description < $b->description) ? -1 : 1;
				}
			}
			usort($jsonDecode,'sort_by_description');
		}
	}
	if ($jsonDataContents != false) :
		echo '<table class="table" id="table-filters"><thead><tr>';
    	echo '<th>Language</th><th>License</th><th>Source</th><th>Dictionary</th><th>SHA256</th><th>Link</th>';
    	echo '</tr></thead><tbody>';
		foreach ($jsonDecode as $jsonItem) :
				if (file_exists('../../dicts/' . "$jsonItem->name" . '.zip') != false) {
    				echo '<tr>';
					if ($jsonItem->name != false) {
						if ($jsonItem->description != false) {
    						echo "<td>$jsonItem->description</td>";
						} else {
    						echo "<td></td>";
						}
						if ($jsonItem->license != false) {
				    		if ($jsonItem->license != false) {
								if ($jsonLicDataContents != false) :
									foreach ($jsonLicDecode as $jsonLicItem) :
										if ($jsonLicItem->id == $jsonItem->license ) {
											if ($jsonLicItem->descriptionUrl != false) {
												echo "<td><form style=\"display: inline\" action=\"$jsonLicItem->descriptionUrl\" method=\"get\"><button class=\"btn btn s-rounded bg-dark btn-success\">$jsonLicItem->name <i class=\"icon icon-link text-secondary\"></i></button></form></td>";
											}
										}
									endforeach;
								endif;
							}
						} else {
    						echo "<td></td>";
						}
						if ($jsonItem->upstreamUrl != false) {
							echo "<td><form style=\"display: inline\" action=\"$jsonItem->upstreamUrl\" method=\"get\"><button class=\"btn btn-action s-rounded bg-dark btn-success\"><i class=\"icon icon-link text-secondary\"></i></button></form></td>";
						} else {
    						echo "<td></td>";
						}
						if ($jsonItem->descriptionUrl != false) {
    						echo "<td><a href=\"$jsonItem->descriptionUrl\" class=\"external-link\">$jsonItem->name</a></td>";
						} else {
    						echo "<td>$jsonItem->name</td>";
						}
						if (file_exists('../../dicts/' . "$jsonItem->name" . '.sha256') != false) {
							echo "<td><form style=\"display: inline\" action=\"dicts/$jsonItem->name" . ".sha256\" method=\"get\"><button class=\"btn btn-action s-rounded bg-dark btn-success\"><i class=\"icon icon-check text-secondary\"></i></button></form></td>";
						} else {
    						echo "<td></td>";
						}
						echo "<td><form style=\"display: inline\" action=\"dicts/$jsonItem->name" . ".zip\" method=\"get\"><button class=\"btn btn-action s-rounded bg-dark btn-success tooltip tooltip-right\" data-tooltip=\"$jsonItem->name" . ".zip\"><i class=\"icon icon-download text-secondary\"></i></button></form></td>";
					}
    				echo '</tr>';
				}
		endforeach;
		echo '</tbody></table>';
	endif;
@endphp
					<div class="columns">
						<div class="column col-6">
							<div class="column col-6">
								<!-- Some content could maybe go here: -->
							</div>
						</div>
						<div class="column col-6"></div>
					</div>
				</div>
				<div class="column col-4">
					<div class="right-side">@include('_includes.sidebar')</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="columns">
				<div class="column col-12">
					<div id="footer"><span class="text-dark"><small>Copyleft <time>{{ $footerDate }}</time> TW3. All trademarks are copyright their various owners.</small></span></div>
				</div>
			</div>
		</div>

	</body>

</html>