@extends('_includes.base')
@section('body')

    <div class="welcome">
        <div class="wrapper">
            <h1>{{ $siteName }}</h1>
            <header>
                <span>{{ $siteDescription }}</span>
            </header>
        </div>
    </div>
	<br />
    <div class="left-side"><main>
        @markdown
        @endmarkdown
    </main></div>

<hr />
<br />

@stop