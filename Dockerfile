
# Define base image
FROM fedora:31
# Install QtWebengine, hunspell and Zip
RUN dnf -q -y --refresh --noautoremove upgrade-minimal && dnf -q -y --nodocs --noautoremove --skip-broken install qt5-qtwebengine hunspell zip
