# QtWebEngine Dictionaries

QtWebEngine dictionary archive files (.bdic) for many languages. Find the spelling dictionary you need for your QtWebEngine based application [here][7b1222f3].

  [7b1222f3]: https://tw3.gitlab.io/qtwebenginedictionaries "qtwebenginedictionaries"

On Linux, the `file` command can be used to identify the file encoding format of a dictionary:

`file -bi en_GB.aff`

For compatibility reasons the UTF-8 standard is required for all dictionaries found here.
To convert a dictionary file to the correct file encoding format, the `iconv` program can be used:

`iconv -f iso-8859-1 -t utf-8 en_GB.aff > en_GB_utf8.aff`

The data in this repository has been compiled using the following resources:

- [https://apps.fedoraproject.org/packages/s/hunspell](https://apps.fedoraproject.org/packages/s/hunspell)
- [https://koji.fedoraproject.org/koji/rpminfo?rpmID=16383761](https://koji.fedoraproject.org/koji/rpminfo?rpmID=16383761)
- [https://www.mirrorservice.org/sites/dl.fedoraproject.org/pub/fedora/linux/releases/31/Everything/x86_64/os/Packages/h/](https://www.mirrorservice.org/sites/dl.fedoraproject.org/pub/fedora/linux/releases/31/Everything/x86_64/os/Packages/h/)
- [https://lh.2xlibre.net/locales/](https://lh.2xlibre.net/locales/)
- [https://www.loc.gov/standards/iso639-2/php/code_list.php](https://www.loc.gov/standards/iso639-2/php/code_list.php)
- [https://webcache.googleusercontent.com/search?q=cache:FkhPekLFHX0J:www.lingoes.net/en/translator/langcode.htm+&cd=1&hl=en&ct=clnk&gl=uk](https://webcache.googleusercontent.com/search?q=cache:FkhPekLFHX0J:www.lingoes.net/en/translator/langcode.htm+&cd=1&hl=en&ct=clnk&gl=uk)
- https://addons.mozilla.org/en-US/firefox/language-tools/
- https://cgit.freedesktop.org/libreoffice/dictionaries/tree/
- https://github.com/titoBouzout/Dictionaries
- https://github.com/LibreOffice/dictionaries
- https://github.com/wooorm/dictionaries
- https://chromium.googlesource.com/chromium/deps/hunspell_dictionaries.git
- https://www.fincher.org/Utilities/CountryLanguageList.shtml

Please [open an issue ticket][a4fd777c] or propose a merge request via [Gitlab][bf87c8cd] if you would like to propose any changes to the work here. Please also do the same if you would like to add a new language.

  [a4fd777c]: https://gitlab.com/TW3/qtwebenginedictionaries/issues "qtwebenginedictionaries issue tracker"
  [bf87c8cd]: https://about.gitlab.com/ "Gitlab"

### Thanks!
